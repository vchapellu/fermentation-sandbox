## Ingredients
- 350 g carrots
- dill, bay leaves, black pepper grains
- water
- 2% salt
## History
- 2024/02/24 1st day of fermentation
- 2024/02/25 Burp ok. Brine overflowed.
- 2024/02/26 Burp ok. Still brine overflowing.
- 2024/02/27 Weak burp. No overflowing.
- 2024/02/28 Weak burp.
- 2024/02/29 Weak burp.
- 2024/03/01 Weak burp.
- 2024/03/03 Little gas activity, no need to burp. 
- 2024/03/06 Little burp, little gas activity, smell ok.
- 2024/03/09 Open and tried. Good! Put in the fridge.

