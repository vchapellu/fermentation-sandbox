## Ingredients
- 293g Brussel sprouts
- 1/2 carrot
- 1/2 garlic clove
- 2cm fresh ginger root
- 2 teaspoons gochugaru

In total 340g ingredients + 310 water = 650g + 2% salt (13g)

## History
- 2023/11/19 1st day of fermentation process
- 2023/11/20 burp ok.  smell ok. Minced garlic tends to get stuck on top of the weight
- 2023/11/21 burp ok. smell ok. 
- 2023/11/22 burp ok. smell ok.
- 2023/11/23 burp ok. smell ok. I turn upside down a couple of times.
- 2023/11/24 burp less sparkling than previous days. smell ok.
- 2023/11/25 still weak burp. smell ok.
- 2023/11/26 weak burp. smell ok.
- 2023/11/27 weak burp. smell ok. Maybe the burp is weak because the temperature is too low. I reposition the jar on the lower shelf that I suppose is warmer due to the spotlight beneath.
- 2023/11/28 weak burp. smell a little more acid than yesterday. The brine is slightly cloudy. I turn the jar upside down. I open the jar and press down the weight to perfectly submerge the sprouts.
- 2023/11/29 weak burp. smell ok.
- 2023/11/30 weak burp. smell ok. I replace the plastic pressing disk with a glass weight.
- 2023/12/01 weak burp. smell ok.
- 2023/12/02 weak burp. smell ok.
- 2023/12/03 weak burp. smell ok.
- 2023/12/04 pH <4.5 let's give em a try! I like them, maybe next time I'll cut them in half so that they ripen more evenly, this time they were a bit less "cooked" in the center.



