## Ingredients
- carrot
- cauliflower
- cucumber
- some onion
- dill and bay leaves
- 2,4% salt

## History
2024/02/26 1st day of fermentation
2024/02/27 Already very active, some water spilled from the container.
2024/02/28 Strong burp. Water spilling.
2024/02/29 Strong burp. Water spilling.
2024/03/01 Strong burp. Some water spilling.
2024/03/02 Strong burp. No water spilling.
2024/03/03 Burp ok.
2024/03/05 Burp ok. Smell ok.
2024/03/06 Burp ok. Smell ok.