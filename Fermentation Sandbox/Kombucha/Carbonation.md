###  F2 first try
- Non amo le bevande molto gassate ma voglio comunque provare a indurre carbonazione. 
- In una bottiglia da 1.3 litri unisco il succo di mezzo limone, 2 cucchiai di sciroppo di rabarbaro e un rametto di rosmarino, unisco il kombucha e mescolo bene. Il kombucha di partenza appena filtrato è già apprezzabilmente frizzante.
- Lascio a fermentare altre 24 ore a temperatura ambiente e poi trasferisco in frigo. La bottiglia (Ikea Korken a collo largo) non è ermetica o la guarnizione non è ben posizionata, posso sentire chiaramente il gas uscire mentre muovo la bottiglia. Brutto segno.
- Dopo sei ore in frigo apro e, come immaginavo, il kombucha non è più frizzante del giorno prima. Il gusto è buono anche se ancora un po' dolce, il rifrattometro indica circa 7°Bx, il pH è sceso ancora leggermente tra 3.5 e 4.0.
- Dopo 3 giorni in frigorifero il gusto è meno dolce ma sempre gradevole.
