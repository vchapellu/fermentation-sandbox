## 1st try from scratch

Il 18 gennaio ho fatto il mio primo tentativo di SCOBY. Una settimana dopo ho trovato delle muffe "pelose" in superficie e il pH era circa 7.0.

### Ingredienti:
- 900 g di acqua
- 4 cucchiaini di tè nero
- 130 g di kombucha biologico
- 65 g di zucchero

### Procedimento:
- Ho fatto bollire dell'acqua del rubinetto e messo in infusione le foglie di tè. 
- Dopo 4 minuti ho filtrato il tè e ho aggiunto lo zucchero.
- Ho lasciato raffreddare a temperatura ambiente.
- Ho messo il tè a temperatura ambiente in un grande vaso di vetro con rubinetto e ho aggiunto il kombucha. Siccome il vaso è molto grande il liquido arrivava proprio al livello del rubinetto quindi ho messo dei dischi di vetro nel boccale abbastanza da alzare il livello al di sopra dell'apertura del rubinetto.
- Ho coperto il vaso con un canovaccio di cotone e l'ho fermato con un elastico.
- Ho posizionato il vaso in un armadio basso in cucina.

### Ipotesi

1. Non ho controllato il pH prima ma il fatto che fosse a 7 dopo una settimana è indice del fatto che era decisamente troppo alto quando ho iniziato il processo. Ho controllato il pH dell'acqua del rubinetto di casa mia, 8.5. 
2. Il kombucha prospera ad una temperatura compresa tra 21°C e 25°C, ho potuto installare il sensore di temperatura nell'armadio solo 5 giorni dopo e la temperatura era di 19.7°C.
3. La muffa era in corrispondenza della guarnizione di sicurezza del rubinetto, un pezzo che poteva non essere perfettamente igienizzato prima del processo.
4. È possibile che lo starter fosse troppo debole.




## 2nd try with a purchased SCOBY

Tutta la letteratura che ho consultato sconsiglia di partire da zero per creare uno SCOBY, sia per ragioni di tempo che per l'elevato tasso di fallimento dato da prodotti commerciali con colture troppo deboli al momento dell'acquisto. Per questi motivi farò il mio secondo tentativo utilizzando uno SCOBY acquistato che arriva in una busta sigillata con 75g di SCOBY e 200g di liquido di coltura. 
Ho fatto tesoro dei miei errori e in questo secondo tentativo ho misurato attentamente il pH in tutte le fasi del processo, utilizzato un contenitore più piccolo e senza rubinetto e ho messo a fermentare il kombucha in un luogo più caldo e con meno oscillazioni di temperatura.
### Ingredienti
- 1300 g di acqua filtrata pH 7.5
- 11 g di the nero in foglie
- 100 g di zucchero di canna 
- 200 g di kombucha 
- SCOBY
### Procedimento
- Ho messo in infusione il the nero in 350 g di acqua per 10 minuti;
- ho aggiunto lo zucchero al the e l'ho fatto sciogliere completamente;
- ho aggiunto 950 g di acqua fredda;
- ho messo tutto in un vaso da 2 litri;
- ho aggiunto il kombucha, misurato il pH (5.0), aggiunto lo SCOBY e nuovamente misurato il pH (4.5);
- ho coperto il vaso con due strati di garza alimentare che ho fermato con un elastico e ho riposto il vaso in uno scaffale alto della cucina la cui temperatura è di 19.8°- 20.8°
### Storia
- Nei primi due giorni non ho notato nessuna attività di gas e nessuna formazione sulla superficie. L'odore è dolce e leggermente acido. Il liquido è molto scuro. Lo SCOBY è stabilmente immerso a 2/3 cm dalla superficie.
- Il terzo giorno compaiono nel vaso dei filamenti e dei gruppi consistenti di materiale marrone (lieviti?).
- Il quarto giorno non ho controllato il vaso per non spostarlo e non disturbare l'eventuale formazione di SCOBY.
- Il quinto giorno il nuovo SCOBY è presente sulla superficie e appena sotto si può notare attività di gas.
- Il sesto e il settimo giorno lo SCOBY si è ispessito, l'odore del kombucha è diventato decisamente più acido e il colore molto più chiaro del liquido di partenza. Il pH è intorno a 4.0. Assaggio e decido di filtrare questo primo kombucha riuscito e di fare una seconda fermentazione con l'aggiunta di aromi.



