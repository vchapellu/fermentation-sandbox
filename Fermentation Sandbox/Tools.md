- glass jars with airtight lid of different sizes. I have bought different types: Weck 850ml with rubber rings and clips, Ikea Korken 1l, Bormioli 500ml, and four quite expensive Lékué jars with vented lid and pressing disk.
- glass weights. Glass is probably the best material for everything around fermentation, non-porous and easy to clean.
- pH indicator test strips